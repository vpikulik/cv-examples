
import numpy as np

from utils import get_laplacian, get_Gx, get_Gy, get_G, noize


def gvf_iteration(field, mu, B, C):
    return mu * get_laplacian(field) - B * field + C


def get_gvf(im_g, mu=1, iterations_count=20):

    Gx = get_Gx(im_g)
    Gy = get_Gy(im_g)
    B = Gx ** 2 + Gy ** 2
    Cx = B * Gx
    Cy = B * Gy

    u = np.copy(Gx)
    v = np.copy(Gy)

    for i in xrange(iterations_count):
        u = gvf_iteration(u, mu, B, Cx)
        v = gvf_iteration(v, mu, B, Cy)

    return u, v
