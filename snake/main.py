
import Image
import sys
import math
import logging

import numpy as np

from memoized_property import memoized_property

import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.cm as cm

from utils import *


logger = logging.getLogger('snake')
logger.handlers = []
log_file = logging.FileHandler('/home/promo/results.log')
log_file.setLevel(logging.INFO)
logger.addHandler(log_file)
logger.setLevel(logging.INFO)
# log_stream = logging.StreamHandler()
# log_stream.setLevel(logging.INFO)


dff_inner = lambda a2, a1: (
    (a2[0] - a1[0]) ** 2 +
    (a2[1] - a1[1]) ** 2
) ** 0.5
dff = lambda a2, a1: dff_inner(
    (float(a2[0]), float(a2[1])),
    (float(a1[0]), float(a1[1]))
)


class Contour(object):

    ALPHA = 1
    BETTA = 1

    DELTAS = (
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 0),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    )

    def __init__(self, points=[], im_g=None, G=None):
        self.points = points
        self.im_g = im_g
        self.G = G

        self.max_x = len(im_g) - 1
        self.max_y = len(G) - 1

    @classmethod
    def from_rectangle(cls, im_g, G, x1, y1, x2, y2, pcount=10):
        points = (
            zip(range(x1, x2, int((x2 - x1) / pcount)), [y1] * pcount) +
            zip([x2] * pcount, range(y1, y2, int((y2 - y1) / pcount))) +
            zip(range(x2, x1, -1 * int((x2 - x1) / pcount)), [y2] * pcount) +
            zip([x1] * pcount, range(y2, y1, -1 * int((y2 - y1) / pcount)))
        )
        return cls(points=points, im_g=im_g, G=G)

    def get_xy(self):
        x = map(lambda item: item[0], self.points)
        y = map(lambda item: item[1], self.points)
        return x + [x[0]], y + [y[0]]

    def E(self):
        en = 0
        for index in range(0, len(self.points)):
            en += self.point_E(index)
        return en

    def point_E(self, index):

        previous = self.points[index - 1]  # Vs-1
        current = self.points[index]  # Vs
        next = self.points[int(math.fmod(index + 1, len(self.points)))]  # Vs+1

        return (
            self.ALPHA * dff(current, previous) ** 2 +
            self.BETTA * (dff(next, current) + dff(previous, current)) ** 2
        ) * 0.5

    def im_E(self):
        en = 0
        for index in range(0, len(self.points)):
            en += self.point_im_E(index)
        return en

    def point_im_E(self, index):
        x, y = self.points[index]
        return self.im_g[y][x] + self.G[y][x] ** 2

    @memoized_property
    def full_E(self):
        en = 0
        for index in range(0, len(self.points)):
            en += self.point_full_E(index)
        return en

    def point_full_E(self, index):
        return self.point_E(index) + 0.1 * self.point_im_E(index)

    def copy(self):
        return self.__class__(points=self.points[:], im_g=self.im_g, G=self.G)

    def new_contour(self, point_index, delta):
        contour = self.copy()
        x = contour.points[point_index][0] + delta[0]
        y = contour.points[point_index][1] + delta[1]
        if x <= self.max_x and y <= self.max_y:
            contour.points[point_index] = (x, y)
            return contour

    def find_new(self, point_index):
        choices = map(
            lambda (contour, delta): (contour.full_E, contour, delta),
            filter(
                lambda (contour, delta): bool(contour),
                map(
                    lambda delta: (self.new_contour(point_index, delta), delta),
                    self.DELTAS
                )
            )
        )
        pre_log = lambda choices: map(lambda (E, c, d): (E, d), choices)
        logger.info('Before sorting: %s', print_list_of_tuples(pre_log(choices)))
        choices = sorted(choices, key=lambda choice: choice[0])
        logger.info('After sorting: %s', print_list_of_tuples(pre_log(choices)))

        logger.info('Choice: (%s, %s), %s', self.points[point_index][0], self.points[point_index][1], print_tuple(choices[0][2]))
        return choices[0][1]


class Snake(object):

    def __init__(self, im_g, G):
        self.im_g = im_g
        self.G = G
        self.step = 0
        self.point = 0
        self.contour = Contour.from_rectangle(im_g, G, 5, 5, 94, 94, 5)
        self.markers = None
        self.energy_x = []
        self.c_energy_y = []
        self.c_max_energy = 0
        self.c_min_energy = 0
        self.i_energy_y = []
        self.i_max_energy = 0
        self.i_min_energy = 0
        self.s_energy_y = []
        self.s_max_energy = 0
        self.s_min_energy = 0
        self.init_plot()

    def init_plot(self):
        plt.ion()
        plt.show()
        self.fig, (self.ax1, self.ax2) = plt.subplots(1, 2)

        self.ax1.imshow(self.G, cmap=cm.gray, origin='upper')

        x, y = self.contour.get_xy()
        self.line = lines.Line2D(x, y, color='red')
        self.ax1.add_line(self.line)

        E = self.contour.E()
        im_E = self.contour.im_E()
        sum_E = self.contour.full_E

        self.energy_line = lines.Line2D([0], [E], color='green')
        self.ax2.add_line(self.energy_line)

        self.image_line = lines.Line2D([0], [im_E], color='blue')
        self.ax2.add_line(self.image_line)

        self.sum_line = lines.Line2D([0], [sum_E], color='red')
        self.ax2.add_line(self.sum_line)

        plt.draw()

    def draw_lines(self):
        x, y = self.contour.get_xy()
        self.line.set_xdata(x)
        self.line.set_ydata(y)

        self.energy_x.append(self.step)

        energy = self.contour.E()
        self.c_max_energy = max(self.c_max_energy, energy)
        self.c_min_energy = min(self.c_min_energy, energy) if self.step > 1 else energy
        self.c_energy_y.append(energy)

        if self.markers:
            self.markers.remove()
        self.markers = self.ax1.scatter(x, y)

        self.energy_line.set_xdata(self.energy_x)
        self.energy_line.set_ydata(self.c_energy_y)

        im_energy = self.contour.im_E()
        self.i_max_energy = max(self.i_max_energy, im_energy)
        self.i_min_energy = min(self.i_min_energy, im_energy) if self.step > 1 else im_energy
        self.i_energy_y.append(im_energy)
        self.image_line.set_xdata(self.energy_x)
        self.image_line.set_ydata(self.i_energy_y)

        sum_energy = self.contour.full_E
        self.s_max_energy = max(self.s_max_energy, sum_energy)
        self.s_min_energy = min(self.s_min_energy, sum_energy) if self.step > 1 else sum_energy
        self.s_energy_y.append(sum_energy)
        self.sum_line.set_xdata(self.energy_x)
        self.sum_line.set_ydata(self.s_energy_y)

        logger.info(u'E: %s, im_E: %s, sum_E: %s', energy, im_energy, sum_energy)

        self.ax2.set_xlim(0, self.step + 10)
        self.ax2.set_ylim(
            min(self.c_min_energy, self.i_min_energy, self.s_min_energy) - 10,
            max(self.c_max_energy, self.i_min_energy, self.s_max_energy) + 10
        )

        plt.draw()

    def next_step(self):
        logger.info('STEP: %s, %s', self.step, self.point)
        self.contour = self.contour.find_new(self.point)

        x, y = self.contour.points[self.point]
        logger.info('I: %s G: %s', self.im_g[x][y], self.G[x][y])

        self.step += 1
        self.point = int(math.fmod(self.step, len(self.contour.points)))
        self.draw_lines()


im_path = 'logo_2.png'
if len(sys.argv) > 1:
    im_path = sys.argv[1]

im_g = load_image(im_path)

Gx = get_Gx(im_g)
Gy = get_Gy(im_g)
G = get_G(Gx, Gy)

snake = Snake(im_g, norm(G))
