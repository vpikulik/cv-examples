
import Image
import numpy as np
from scipy.signal import convolve2d
from scipy.ndimage.filters import gaussian_filter, laplace


def load_image(im_path):
    im_rgba = np.array(Image.open(im_path))
    return get_grey(im_rgba)


def norm(np_im, max_value=255):
    mx = np_im.max()
    mn = np_im.min()
    return ((np_im - mn) / (mx - mn)) * max_value


def get_grey(im_rgba):
    im_r = im_rgba[:, :, 0]
    im_g = im_rgba[:, :, 1]
    im_b = im_rgba[:, :, 2]
    return 0.2989 * im_r + 0.5870 * im_g + 0.1140 * im_b


def noize(np_im, sigma=3):
    return gaussian_filter(np_im, sigma)


def get_Gx(im_g):
    sobel_x = np.array([[1, 0, -1], [2, 0, -2],  [1, 0, -1]])
    # gx = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])
    return convolve2d(im_g, sobel_x, mode='same')


def get_Gy(im_g):
    sobel_y = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])
    # gy = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
    return convolve2d(im_g, sobel_y, mode='same')


def get_G(Gx, Gy):
    return (Gx ** 2 + Gy ** 2) ** 0.5


def get_laplacian(im_g):
    return laplace(im_g)


print_tuple = lambda tpl: u'({})'.format(', '.join(map(unicode, tpl)))


def print_list_of_tuples(data):
    return print_tuple(map(print_tuple, data))
